<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    // use DatabaseTransactions;
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function getHeader()
    {   
        $user = new User();
        $userdefault = $user->where('username','defaulttester')->get();
        if(is_null($user)){
            if(empty($userdefault))
            {
                $user = factory(User::class)->create([
                    'username'=>'defaulttester',
                    'password' => bcrypt('senhateste'),
                    'email'=>'teste@email.com',
                    'type'=>'user',
                    'access'=>'restrict',
                ]);
            }else
            {
                $user = $userdefault;
            }
        }



        $data=[
            'email'=>'teste@email.com',
            'password'=>'senhateste',
        ];


        $this->post('auth/login',$data);


        $data = $this->response->getContent();

        $token = json_decode($data)->token;

        return [
            'Authorization'=>'Bearer'.$token,
        ];

    }
}
