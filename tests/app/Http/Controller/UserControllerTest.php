<?php

namespace App\Http\Controllers;
use App\User;

class UserControllerTest extends \TestCase
{
	public function testSave()
	{
		$user = new User();
		$headers = $this->getHeader();

		$data = [
			'name' =>     'roberto',
			'username'=>  'roberto',
			'password' => 'roberto123',
			'email' => 	  'roberto@email.com',
			'type' =>     'user',
			'access' =>   'restrict', 
		];

		$this->post('/users', $data, $headers);

		//assert
		$this->seeStatusCode(200);		
	}

	public function testUpdate()
	{
		$user = new User();
		$headers = $this->getHeader();
		//cria usuário a ser editado
		$user = factory(User::class)->create([
                    'username'=>'roberta',
                    'password' => bcrypt('senhateste'),
                    'email'=>'roberta@email.com',
                    'type'=>'user',
                    'access'=>'restrict',
                ]);


		$data = [
			'id' => $user->id,
			'name' =>     'roberti',
			'username'=>  'roberti',
			'password' => 'roberto123',
			'email' => 	  'roberti@email.com',
			'type' =>     'user',
			'access' =>   'restrict', 
		];

		$this->put('/users/'.$user->id, $data, $headers);

		//assert
		$this->seeStatusCode(200);	

	}

	public function testdelete()
	{
		$user = new User();
		$headers = $this->getHeader();
		//cria usuário a ser editado
		$user = factory(User::class)->create([
                    'username'=>'robertao',
                    'password' => bcrypt('senhateste'),
                    'email'=>'robertao@email.com',
                    'type'=>'user',
                    'access'=>'restrict',
                ]);


		$this->delete('/users/'.$user->id, [], $headers);
		//assert
		$this->seeStatusCode(200);	

	}
	public function limpando($email)
	{
		$user = new User();
		$headers = $this->getHeader();

		$achei = $user->where('email', $email)->get();
		
		$this->delete('/users/'.$achei->id, [], $headers);
		
	}	

}	