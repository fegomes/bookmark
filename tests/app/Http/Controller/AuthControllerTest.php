<?php

namespace App\Http\Controller;

use App\User;

class AuthControllerTest extends \TestCase
{

	public function testLogin()
	{	
		$data=[
			'email'=>'authTester@email.com',
			'password'=>'senhateste',
		];

		$userdefault = new User();
		if(!$userdefault->where('username','authTester')->get())
		{
			$user = $data;
			$user['username'] = 'authTester' ;
			$user['password'] = bcrypt($user['password']);
			$user['email'] = 'authTester@email.com';
			$user['type'] = 'user';
			$user['access'] = 'restrict';
			factory(User::class)->create($user);
		}
		$this->post('auth/login', $data);

		//asserts
		$this->seeStatusCode(200);
		$this->seeJson([
			'username'=>'authTester',
			]);
	}


	public function testLoginWithEmail(){
		$data=[
		'email'=>'authTester@email.com',
		'password'=>'senhateste',
		];
		$userdefault = new User();
		if(!$userdefault->where('username','authTester')->get())
		{
			$user = $data;
			$user['username'] = 'authTester' ;
			$user['password'] = bcrypt($user['password']);
			$user['email'] = 'authTester@email.com';
			$user['type'] = 'user';
			$user['access'] = 'restrict';
			factory(User::class)->create($user);
		}
		$this->post('auth/login', $data);

		//asserts
		$this->seeStatusCode(200);
		$this->seeJson([
			'email'=>'authTester@email.com',

			]);
	}

	public function testCantLogin(){
		$data=[
		'username'=>uniqid(),
		'password'=>'senhateste',
		];

		$this->post('auth/login', $data);

		//asserts
		$this->seeStatusCode(401);

	}
}