<?php

namespace App\Http\Controllers;
use App\Bookmark;

class BookControllerTest extends \TestCase
{

	public function testSave()
	{
		$headers = $this->getHeader();

		$data = [
			'user_id' => '1',
			'description'=>'descricaooo',
			'url' => 'www.google.com',
		];

		$this->post('/bookmarks', $data, $headers);

		//assert
		$this->seeStatusCode(200);
	}

	public function testUpdate()
	{
		//Cria o bookmark a ser editado
		$bookmark = new Bookmark();
        $bookmark->user_id =  '1';
        $bookmark->description = 'testUpdate';
        $bookmark->url = 'testUpdate';
        if(!$bookmark->save()){
            throw new Exception("Error!",400);
        }

		$headers = $this->getHeader();

		$data = [
			'id'=>$bookmark->id,
			'user_id'=>'1',
			'description'=>'descricao',
			'url' => 'www.testgoogle.com',
		];

		$this->put('/bookmarks/'.$bookmark->id, $data, $headers);

		//assert
		$this->seeStatusCode(200);
	}

	public function testDelete()
	{
		//Cria o bookmark a ser editado
		$bookmark = new Bookmark();
        $bookmark->user_id =  '1';
        $bookmark->description = 'testUpdate';
        $bookmark->url = 'testUpdate';
        if(!$bookmark->save()){
            throw new Exception("Error!",402);
        }

		$headers = $this->getHeader();

		$this->delete('/bookmarks/'.$bookmark->id, [] ,$headers);

		$this->seeStatusCode(200);
	}
}
