<?php

namespace app\Http\Requests;

use App\Http\Requests\AuthRequest;

class BookmarkRequest extends AuthRequest
{
	public function rules()
	{
		return[
			'user_id'=>'required',
			'description'=>'required',
			'url'=>'required|max:175',
		];
	}
}