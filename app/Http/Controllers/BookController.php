<?php
 /**
 * 
 */
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Response;
use App\Http\Requests\BookmarkRequest;
use App\Bookmark;
use App\User;



 class BookController extends Controller
 {
    protected $bookmark;
    protected $request;

    public function __construct(Request $request)
    {
        $this->bookmark = new Bookmark();
        $this->request = $request;
    }

 	public function index()
 	{     
 		return $this->bookmark->orderBy('user_id')->get();
 	}

 	 public function show($id)
 	{
 		return $this->bookmark->where('user_id', $id)->get();
 	}

 	public function save(BookmarkRequest $request)
 	{
 		 try {
            $params = $request->only('description','url','user_id');
            $bookmark = new Bookmark();
            // $bookmark->user_id = $this->request->user->id;
            $bookmark->user_id = $params['user_id'];
            $bookmark->description = $params['description'];
            $bookmark->url = $params['url'];
            if(!$bookmark->save()){
                throw new Exception("Error!",400);
            }
            
            return  Response::json('Criado com Sucesso', 200);
        } catch (Exception $e)
        {
            return Response::json($e->getMessage(), $e->getCode());
        }
 	}

    public function update(BookmarkRequest $request,$id)
    {   
       try {
            $params = $request->only('description','url');
            
            $this->bookmark = $this->bookmark->find($id);
            $this->bookmark->description = $params['description'];
            $this->bookmark->url = $params['url'];
            
            if(!$this->bookmark->save())
            {
                throw new Exception("Error!",400);
            }
            
            return  Response::json('Alterado com Sucesso', 200);
        } catch (Exception $e) 
        {
            return Response::json($e->getMessage(), $e->getCode());
        }
    }

    public function destroy($id)
    {   
       try {
            $this->bookmark = $this->bookmark->find($id);

            if(!$this->bookmark->delete())
            {
                throw new Exception("Error!",400);
            }
            
            return  Response::json('Removido com Sucesso.', 200);
        } catch (Exception $e)
        {
            return Response::json($e->getMessage(), $e->getCode());
        }
    }

 }