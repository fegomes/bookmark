<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function login(AuthRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials))
            {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e)
        {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return $this->getUser($token);
    }

    public function getUser($token)
    {
    	$user = Auth::user();

    	return compact('token', 'user');
    }

    public function logout()
    {
        $result = JWTAuth::invalidate(JWTAuth::getToken());
        if (!$result)
        {
            return response()->json(['error'=>'falha ao deslogar'],403);
        }
        return response()->json('ok');
    }
}
