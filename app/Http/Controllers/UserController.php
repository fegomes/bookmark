<?php
 /**
 * 
 */
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Response;
use App\Http\Requests\BookmarkRequest;
use App\User;



class UserController extends Controller
{
    protected $user;
    protected $request;
    
    public function __construct(Request $request)
    {
        $this->user = new User();
        $this->request = $request;
    }


	public function index()
	{
		return $this->user->orderBy('id')->get();
	}

	public function show($id)
	{
		return $this->user->find($id);
	}

	public function save()
 	{
 		try {
            $params = $this->request->only('name','username','password','email','type','access');

            $this->user->name = $params['name'];
            $this->user->username = $params['username'];
            $this->user->password = bcrypt($params['password']);
            $this->user->email = $params['email'];
            $this->user->type = $params['type'];
            $this->user->access = $params['access'];
            
            if(!$this->user->save())
            {
                throw new Exception("Error!",400);
            }
            
            return  Response::json('Criado com Sucesso', 200);
        } catch (Exception $e)
        {
            return Response::json($e->getMessage(), $e->getCode());
        }

 	}

	public function update($id)
 	{
 		try {
            $params = $this->request->only('name','username','password','email','type','access');

            $this->user = $this->user->find($id);
            $this->user->name = $params['name'];
            $this->user->username = $params['username'];
            $this->user->password = bcrypt($params['password']);
            $this->user->email = $params['email'];
            $this->user->type = $params['type'];
            $this->user->access = $params['access'];
            
            if(!$this->user->save())
            {
                throw new Exception("Error!",400);
            }
            
            return  Response::json('Atualizado com Sucesso', 200);
        } catch (Exception $e)
        {
            return Response::json($e->getMessage(), $e->getCode());
        }

 	} 	

	public function destroy($id)
 	{
 		try {
            $this->user = $this->user->find($id);
            
            if(!$this->user->delete())
            {
                throw new Exception("Error!",400);
            }
            
            return  Response::json('Removido com Sucesso', 200);
        } catch (Exception $e)
        {
            return Response::json($e->getMessage(), $e->getCode());
        }

 	}

}    