<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username',30)->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type');
            $table->string('access');
            $table->rememberToken();
            $table->timestamps();
        });    

    factory(User::class)->create([
        'name' => 'admin',
        'username' => 'admin',
        'email' => 'admin@admin.com',
        'password' => bcrypt('password123'),
        'type' => 'adm',
        'access' => 'full',

    ]); 
    factory(User::class)->create([
        'name' => 'tester',
        'username' => 'tester',
        'email' => 'tester@m4u.com',
        'password' => bcrypt('m4u'),
        'type' => 'user',
        'access' => 'restrict',

    ]); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
