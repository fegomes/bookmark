# Instalação: #
 - No diretório da aplicação e utilizar o comando: **"php composer.phar install"** com isso as dependências do projeto serão instaladas. 
 - Criação de um banco Mysql com o nome: **"bookmarkDB"**. 
 - No diretório da aplicação rodar o comando: **"php artisan migrate"** assim criando as tabelas necessárias. 
 - No diretório da aplicação rodar o comando: **"php artisan serve"** para inicialização do servidor do Laravel. 

Para acessar basta ir ao diretório bookmark_front e acessar o arquivo index.html

Usuário: admin@admin.com
Senha: password123

- Para rodar os testes unitários basta entrar no diretório da aplicação e rodar o phpunit. Que fica no vendor/phpunit/phpunit/phpunit.
	Para facilitar você pode rodar o seguinte comando linux: alias phpunit='vendor/bin/phpunit'

	Em seguida basta executar: phpunit

- Laravel 5.3
- PHP
- Mysql
- JWT Authentication
- Testes unitários das APIS
- Bootstrap
- AngularJS