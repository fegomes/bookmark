<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function()
{
    return View('welcome');
});


Route::group(['middleware' => 'cors'], function()
{        	
	Route::post('auth/login', 'AuthController@login');

	Route::group(['middleware'=>'jwt.auth'],function()
	{
							#####USER#####
		Route::get('/users', 		'UserController@index');
		Route::get('/users/{id}',   'UserController@show');
		Route::post('/users', 		'UserController@save');
	    Route::put('/users/{id}', 	'UserController@update');
	    Route::delete('/users/{id}','UserController@destroy');

							#####BOOKMARKS#####
		Route::get('/bookmarks', 		'BookController@index');
		Route::get('/bookmarks/{id}',   'BookController@show');
		Route::post('/bookmarks', 		'BookController@save');
	    Route::put('/bookmarks/{id}', 	'BookController@update');
	    Route::delete('/bookmarks/{id}','BookController@destroy');

		Route::post('auth/logout', 'AuthController@logout');
	});

});

